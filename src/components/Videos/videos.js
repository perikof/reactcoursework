import React from 'react';
import VideosList from '../Widget/videos/videosList';
import '../Videos/videos.css';


class Videos extends React.Component {

    render() {

        return (
            <div> 
                <VideosList
                    type="card"
                    title={true}
                    loadmore={true}
                    start={0}
                    amount={5}
                />
            </div>


        )
    }
}
export default Videos