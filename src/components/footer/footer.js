import React from 'react';
import  './footer.css';
import { NavLink } from "react-router-dom";


class Footer extends React.Component {

    render() {

        return (
            <div>
    <div className="ikonlar">
       <div className="container">
                <div className="text-center">
    			<a href="#"><span className="fa fa-facebook-square"></span></a>
    			<a href="#"><span className="fa fa-twitter-square"></span></a>
    			<a href="#"><span className="fa fa-linkedin-square"></span></a>
    			<a href="#"><span className="fa fa-google-plus-square"></span></a>
                <p>Copyright © 2018. All rights reserved!</p>
                </div>
    	</div>
    </div>
            </div>
        )
    }
}
export default Footer