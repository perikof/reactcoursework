import React from 'react';
import './postdata.css';
import FontAwesome from 'react-fontawesome'

class PostDate extends React.Component {

    render() {

        return (
            <div className="container DateAuthor">
                <div className="postDate"><span><FontAwesome name="clock-o"/> </span>
                     {this.props.data.date}
                </div>
                <div className="postAuthor"><span>Author: </span> 
                    {this.props.data.author}
                </div>
            </div> 
        )
    }
}
export default PostDate