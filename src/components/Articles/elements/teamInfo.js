import React from 'react';
import  './teamInfo.css';

class TeamInfo extends React.Component {

    render() {

        return (
            <div className="container teamInfo">
            <p className="MyStl">
            <div className="teamInfo_team">
                    {this.props.team.city}
                    {this.props.team.name}
                </div>
            </p>
                <div className="teamInfo_logo"
                    style={{
                        background: `url('/images/teams/${this.props.team.logo}')`,
                        }}
                >
                </div>

                        <p>
                <div className="teamInfo_games">
                  <span className="green">W: {this.props.team.stats[0].wins};</span>
                  <span className="red">L: {this.props.team.stats[0].defeats};</span>
                </div>
                </p>
            </div>


        )
    }
}
export default TeamInfo