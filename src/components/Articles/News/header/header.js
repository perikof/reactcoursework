import React from 'react';
import HeaderSlider from '../../../Widget/headerSlider/headerSlider';
import NewsList from '../../../Widget/newsList/newsList';


class Header extends React.Component {

    render() {

        return (
            <div>
                <HeaderSlider
                    type="featured"
                    start={0}
                    amount={3}
                    settings={{
                        dots: false,
                        infinite: true,
                        speed: 500,
                    }}
                />

                <NewsList
                    start={5}
                    amount={5}
                    type="card"
                    loadmore={true}
                />

                
            </div>
        )
    }
}
export default Header