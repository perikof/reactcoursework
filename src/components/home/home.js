import React from 'react';
import HeaderSlider from '../Widget/headerSlider/headerSlider';
import VideosList from '../Widget/videos/videosList';
import NewsList from '../Widget/newsList/newsList';



class Home extends React.Component {

    render() {

        return (
            <div>
                <HeaderSlider
                    type="featured"
                    start={0}
                    amount={3}
                    settings={{
                        dots: false,
                        infinite: true,
                        speed: 500
                    }}
                />
                <NewsList
                    start={3}
                    amount={3}
                    type="card"
                    loadmore={true}
                />
                <VideosList
                    type="card"
                    title={true}
                    loadmore={true}
                    start={0}
                    amount={3}
                />
            </div>


        )
    }
}
export default Home