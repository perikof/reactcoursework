import React from 'react';
import style from './header.css';
import {NavLink} from "react-router-dom";
import SideNavigatoin from "./sideNav/sideNav";
import FontAwesome from "react-fontawesome";


class Header extends React.Component {

    navBars = () => (
        <div className={style.bars}>
            <FontAwesome name="bars"
                         onClick={this.props.onOpenNav}
                         style={{
                             color: '#dfdfdf',
                             padding: '10px',
                             cursor: 'pointer'
                         }}
            />
        </div>
    )

    render() {

        return (
            <header className={style.header}>
                    <nav className="navbar navbar-expand-lg navbar-dark mx-background-top-linear">
                        <div className="container">
                            <NavLink className="navbar-brand MyStyleBrand" to="/"><i class="fas fa-basketball-ball"></i>
                            <span>
                                <font size="100" color="blue" face="Comic Sans MS">N</font>
                                <font size="100" color="white" face="Comic Sans MS">B</font>
                                <font size="100" color="red" face="Comic Sans MS">A</font>
                                <font size="100" color="white" face="Comic Sans MS">.COM</font>
                                </span></NavLink>
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item myStyle active"><NavLink className="nav-link" to="/">
                                <i class="fas fa-home"></i><span
                                    className="sr-only">(current)</span></NavLink></li>
                            </ul>
                            <div className="burger myStyle">
                                <SideNavigatoin {...this.props} />
                                <div className={style.headerOpt}>
                                    {this.navBars()}
                                </div>
                            </div>
                        </div>
                    </nav>
                    <div className='line'></div>
            </header>
        )
    }
}
export default Header