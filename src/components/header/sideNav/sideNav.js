import React from 'react';
import SideNav from "react-simple-sidenav";
import SideNavItems from "../sideNav/sideNavItems";


class SideNavigatoin extends React.Component {

    render() {

        return (
            <div>
                <SideNav
                    showNav={this.props.showNav}
                    onHideNav={this.props.onHideNav}
                    navStyle={{
                        background: '#212529',
                        maxWidth: '250px',
                        padding: '15px',
                    }}

                >
                    <SideNavItems />
                </SideNav>
            </div>
        )
    }
}

export default SideNavigatoin